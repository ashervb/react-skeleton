import { fork, all, call, put, takeLatest, select } from 'redux-saga/effects'

import { Action } from 'src/types'
import userAPI from 'src/api/users'
import storage from 'src/services/storage'

const types = {
  SIGNUP_START: 'AUTH/SIGNUP_START',
  SIGNUP_SUCCESS: 'AUTH/SIGNUP_SUCCESS',
  SIGNUP_FAILURE: 'AUTH/SIGNUP_FAILURE',
  LOGIN_START: 'AUTH/LOGIN_START',
  LOGIN_SUCCESS: 'AUTH/LOGIN_SUCCESS',
  LOGIN_FAILURE: 'AUTH/LOGIN_FAILURE',
  LOGOUT: 'AUTH/LOGOUT'
}

const actions = {
  signup: (email: string, password: string, password_confirmation: string) => ({ type: types.SIGNUP_START, payload: { email, password, password_confirmation } }),
  login: (email: string, password: string) => ({ type: types.LOGIN_START, payload: { email, password } }),
  logout: () => ({ type: types.LOGOUT, payload: {} })
}

const initialState: { user: any, isLoading: boolean, error: any } = {
  user: null,
  isLoading: false,
  error: null,
}

const reducer = (state = initialState, action: Action<any>) => {
  switch (action.type) {
    case types.SIGNUP_START:
    case types.LOGIN_START:
      return { ...state, isLoading: true, error: null }

    case types.SIGNUP_SUCCESS:
    case types.LOGIN_SUCCESS:
      return { ...state, isLoading: false, user: action.payload }

    case types.SIGNUP_FAILURE:
    case types.LOGIN_FAILURE:
      return { ...state, isLoading: false, error: action.payload }

    case types.LOGOUT:
      return { ...state, user: null }

    default:
      return state
  }
}

//
// Subroutines
//

function* userLogin(action: Action<{email: string, password: string}>) {
  const { payload: { email, password } } = action
  let resp

  try {
    resp = yield call(userAPI.sign_in, email, password)
    yield put({type: types.LOGIN_SUCCESS, payload: resp.body})
    const token = resp.body.spree_api_key

    // Store state in localStorage
    const state = yield select()
    storage.save(state)
  } catch (e) {
    yield put({type: types.LOGIN_FAILURE, payload: e.response.body, error: true})
  }
}

function* userLogout(action: Action<{}>): any {
  storage.clear()
  window.location.pathname = "/"
}

function* userSignUp(action: Action<{ email: string, password: string, password_confirmation: string }>) {
  const { email, password, password_confirmation } = action.payload

  try {
    const resp = yield call(userAPI.sign_up, email, password, password_confirmation)
    yield put({type: types.SIGNUP_SUCCESS, payload: resp.body})

    // Store state in localStorage
    const state = yield select()
    storage.save(state)
  } catch (e) {
    yield put({type: types.SIGNUP_FAILURE, payload: e.response.body, error: true})
  }
}

//
// Watchers
//

function* watchUserLogin() {
  yield takeLatest(types.LOGIN_START, userLogin)
}

function* watchUserLogout() {
  yield takeLatest(types.LOGOUT, userLogout)
}

function* watchUserSignUp() {
  yield takeLatest(types.SIGNUP_START, userSignUp)
}

export default {
  types,
  actions,
  reducer,
  saga: function*() {
    yield all([
      fork(watchUserLogin),
      fork(watchUserLogout),
      fork(watchUserSignUp),
    ])
  }
}
