import * as React from "react"

import { IField } from '../../types'

export default ({ name, placeholder, type = "text", size, hint, label, onChange, defaultValue, value, required = true, checked, options = [] } : IField): React.ReactElement<"div"> => {
  return (
    <div className={`field field-${type}`}>
      <label>
        {label && label}
        {label && required && <abbr className="required" title="required">*</abbr>}
        {type == "select"
          ? <select onChange={onChange} value={value} required={required}>
              {options.map((opt,i) => <option key={i} value={opt.value}> {opt.label}</option>)}
            </select>
          : <input
              {...(["checkbox", "radio"].indexOf(type)> -1 ? { checked } : {})}
              name={name}
              size={size}
              type={type}
              placeholder={placeholder && `${placeholder}${required && '*'}`}
              onChange={onChange}
              value={value}
              defaultValue={defaultValue}
              required={required}
            />
        }
      </label>
      {hint && <span className='hint'>{hint}</span>}
    </div>
  )
}
