var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var path = require("path");

const env = {
  NODE_ENV: JSON.stringify('production'),
}

module.exports = {
	devtool: 'source-map',
	entry: [
    './src/index.tsx'
	],
	output: {
		filename: '[name].[chunkhash:8].js',
    publicPath: "/",
		path: path.resolve(__dirname, "dist")
	},
	resolve: {
		extensions: ['.ts', '.tsx', '.js', 'json', '.jsx']
	},
	module: {
    rules: [
      // Typescript
      { test: /\.tsx?$/, loader: "awesome-typescript-loader" },

      // JavaScript
      { enforce: "pre", test: /\.js$/, loader: "source-map-loader" },

      // CSS
      { test: /\.scss$/, enforce: "pre", loader: 'import-glob-loader' },

      { test: /\.scss$/, use: ['style-loader',  'css-loader', 'sass-loader', 'postcss-loader?parser=postcss-scss' ] },

      // Fonts
      {
        test: /\.woff(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [{
          loader: 'url-loader',
          options: {
            name: 'assets/[hash].[ext]',
            limit: 65000,
            mimetype: 'application/font-woff',
          }
        }]
      },
      {
        test: /\.woff2(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [{
          loader: 'url-loader',
          options: {
            name: 'assets/[hash].[ext]',
            limit: 65000,
            mimetype: 'application/font-woff2',
          }
        }]
      },
      {
        test: /\.(gif|jpe?g|png|wav|mp3)(\?\S*)?$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: 'assets/[hash].[ext]',
          },
        }],
      },

      // SVG
      {
        test: /\.svg(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [{
          loader: 'url-loader',
          options: {
            name: 'assets/[hash].[ext]',
            limit: 80000,
            mimetype: 'image/svg+xml',
          }
        }]
      },
    ]
	},
	plugins: [
		new webpack.DefinePlugin({
      'process.env': env
		}),
		new ExtractTextPlugin("[name].[contenthash:8].css"),
		new HtmlWebpackPlugin({
			inject: true,
			template: 'index.html'
		}),
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.optimize.UglifyJsPlugin({
			sourceMap: true,
			compress: {
				screw_ie8: true, // React doesn't support IE8
				warnings: false
			},
			mangle: {
				screw_ie8: true
			},
			output: {
				comments: false,
				screw_ie8: true
			}
		}),
	]
};
