const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const env = {
  // ... dev env variables go here
}

module.exports = {
  entry: "./src/index.tsx",
  output: {
    filename: "bundle.js",
    publicPath:  'http://localhost:3000/',
    path: __dirname + "/dist"
  },

  // Enable sourcemaps for debugging webpack's output.
  devtool: "source-map",

  resolve: {
    // Add '.ts' and '.tsx' as resolvable extensions.
    extensions: [".ts", ".tsx", ".js", ".json", ".jsx"],
    modules: [ path.join(__dirname, './src'),  path.join(__dirname, './node_modules')  ]
  },

  module: {
    rules: [
      // Typescript
      { test: /\.tsx?$/, loader: "awesome-typescript-loader" },

      // JavaScript
      { enforce: "pre", test: /\.js$/, loader: "source-map-loader" },

      // CSS
      { test: /\.scss$/, enforce: "pre", loader: 'import-glob-loader' },

      { test: /\.scss$/, use: ['style-loader',  'css-loader', 'sass-loader', 'postcss-loader?parser=postcss-scss' ] },

      // Fonts
      {
        test: /\.woff(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [{
          loader: 'url-loader',
          options: {
            name: 'assets/[hash].[ext]',
            limit: 65000,
            mimetype: 'application/font-woff',
          }
        }]
      },
      {
        test: /\.woff2(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [{
          loader: 'url-loader',
          options: {
            name: 'assets/[hash].[ext]',
            limit: 65000,
            mimetype: 'application/font-woff2',
          }
        }]
      },

      {
        test: /\.(gif|jpe?g|png|wav|mp3)(\?\S*)?$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: 'assets/[hash].[ext]',
          },
        }],
      },

      // SVG
      {
        test: /\.svg(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [{
          loader: 'url-loader',
          options: {
            name: 'assets/[hash].[ext]',
            limit: 80000,
            mimetype: 'image/svg+xml',
          }
        }]
      },
    ]
  },

	plugins: [
		new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({ 'process.env': env }),
		new webpack.NamedModulesPlugin(),
		new HtmlWebpackPlugin({
			inject: true,
			template: 'index.html'
		})
	],

	devServer: {
		hot: true,
    historyApiFallback: { index: '/' },
    host: "0.0.0.0",
    port: "3000",
		//Enable this if you want to never refresh (this allows hot-reloading app.tsx, but won't auto-refresh if you change index.tsx)
		//hotOnly: true
	}
}
